from .views import GetListMedicineSearchView
from django.urls import path

app_name = 'search2'

urlpatterns = [
    path("search", GetListMedicineSearchView.as_view()),
]